% !TEX root = ../FnTIR2021-Rec.tex

%\section{Beyond Traditional \ac{VAE}-Based Recommendation Methods}
\chapter{Application of hyperparameter optimization method}
\label{sec:othersmethods}

\section{Comparison of optimization methods}
\label{sec:compare}

Grid search uses the idea of exhaustive method, so the algorithm implementation is relatively simple, and it is usually used in low-dimensional parameter spaces. This makes grid search have high requirements for the parameter value range. Thus, it can only play a role when the parameter value is limited. Moreover, it is not able to detect the global optimum of continuous parameters. When the dimension of the parameters increases, the complexity of the grid search will increase exponentially. Due to the exhaustive method, the resource and time consumption of grid search is huge.

Random search changes the exhaustive strategy in algorithm, and uses random extraction, which makes random search more efficient. Random search is applicable to almost all parameter types. For deep learning, one of the biggest characteristic of random search is parallelism. Since the search process of random search is independent each time, the previous sampling configuration is not used, which makes it difficult to find the global optimal result in the search process.

Bayesian optimization is an excellent algorithm for prior search experience. It uses the information of the previous sampling configuration in each iteration, which makes its results more trustworthy.The inability to parallelize directly and poor scalability for high-dimensional spaces are its major drawbacks.The Bayesian optimization method can be divided into three parts according to the different surrogate models: BO-GP, SMAC, TPE.

Bayesian optimization based on Gaussian process is a classic algorithm. Due to its simplicity and practicality, BO-GP method is widely used in machine learning. However, it is mainly suitable for continuous and discrete hyperparameter rather than supporting conditional hyperparameter and categorical hyperparameter, while SMAC and TPE methods are useful for almost all hyperparameter types.

Population-based algorithms generally include classic black box optimization methods such as evolutionary algorithms, particle swarm optimization, etc..\cite{jaderberg2017population} proposed population-based training, which does not use the parameters of the evolutionary model, but uses standard optimization techniques (such as gradient descent) to partially train them. Therefore, it is slightly different from evolutionary algorithm. In our discussion, we will also elaborate on them respectively.

The population-based algorithm is based on the organisms in nature, include evolutionary algorithms, particle swarm optimization and ant colony algorithms, etc., all of which belong to meta-heuristic algorithms.They perform well in a large configuration space and are suitable for almost all parameter types.But for each algorithm, there are unique advantages and disadvantages.Among evolutionary algorithms, genetic algorithm and covariance matrix adaptation evolution strategy are most used in hyperparameter optimization.

Genetic algorithm is very simple in concept. It updates the population through mutation and combination to obtain better parameter configuration.Based on this mutation mechanism, genetic algorithm is not easy to fall into local optimization, and it reflects better global search performance.Genetic algorithm also has good scalability and is easy to combine with other technologies \citep{mahfoud1995parallel,pelikan1999boa}.Parallel genetic algorithm can guarantee the application on massively parallel computers.However, premature convergence is a problem in almost all genetic algorithms. Researchers generally increase diversity through selection pressure to avoid falling into local optimality. The introduction of the two hyperparameters of crossover and mutation operators is another problem of genetic algorithm. Whether it can generate adaptive crossover and mutation operators will be the next research direction \citep{katoch2020review}. 

The covariance matrix adaptive evolution strategy \citep{hansen2001completely}is a advanced optimizer for continuous black box functions.It has low computational cost and supports parallel computing locally.However, the current research requires further in-depth comparison between CMA-ES and other algorithms, and the extensibility of other models remains to be considered \citep{loshchilov2016cma}.

In the particle swarm optimization algorithm, the movement of a particle is not only affected by its local most famous position, but also by the most famous position recognized by other particles in the swarm.The particle swarm optimization algorithm is very sensitive to the position of the initial particle and the maximum particle swarm velocity, while the inertial weight and acceleration coefficient are very sensitive to the particle swarm velocity \citep{wang2019influence}. If the population is not properly initialized, the algorithm is very likely to fall into a local optimum \citep{sengupta2019particle}. However, the excellent performance in parallelization \citep{chu2005parallel} and flexibility makes the PSO algorithm very suitable in the field of hyperparameter optimization.

The ant colony algorithm finds the optimal solution by updating the pheromone. Because the search process uses a distributed computing method, multiple individuals perform parallel computing at the same time, which greatly improves the computing power and operating efficiency of the algorithm.However, ant colony algorithm is rarely used in hyperparameter optimization. The reason may be that the initial pheromone is averaged and the initial convergence speed is slow. According to the random method to complete the construction of the solution, it is very easy to fall into the situation of local optimization, which is unacceptable for hyperparameter optimization \citep{luo2020research}.

The population-based training combines parallel search method and sequential optimization method. Each training run will periodically perform asynchronous evaluation of its performance, and replace the poorly performing models. Moreover, it utilizes the information sharing of concurrently running optimization processes in the group, and allows online transfer of parameters and hyperparameters between performance-based group members so that parameters and hyperparameters can be trained together. This makes the learning speed much faster.However, most of the implementation of PBT adopts the glass box method, which brings additional restrictions to the realization of the neural network model \citep{li2019generalized}.

Multi-Armed Bandits is slightly different from the previous method because it incorporates an early stopping strategy. This is based on the study of the learning curve. The early stopping strategy allows the bandit algorithm to terminate those poorly performing experiments early.The successive halving method is based on the  non-stochastic best-arm identification problem \citep{karnin2013almost}.It has good robustness and versatility.But successive halving algorithms suffer from the  "$n\quad vs.\quad B/n$" problem.The hyperband algorithm can solve such problems very well.Hyperband has strong real-time performance, flexibility and scalability for high-dimensional spaces, but because it has not learned from the previously sampled configuration, it may produce poor results.The BOHB algorithm improves this part of the problem. It combines Bayesian optimization and hyperband to enable the algorithm to have good robustness and versatility, as well as parallelism. The BOHB algorithm has strong performance and converges to a better configuration in advance than TPE and SMAC. But it also has shortcomings.The algorithm need to define a budget before using BOHB. A small budget should produce a cheap approximation for an expensive objective function. If the evaluation of a small budget is misleading or too noisy to really tell you anything about the performance of the full budget, then BOHB's Hyperband Component will be wasted.

The pros and cons of the hyper-parameter optimization algorithms and its application involved in this paper are summarized in \ref{tab:1}.

\begin{table}
	%\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
	\caption{Comparison of hyper-parameter optimization methods}
	\label{tab:1}       % Give a unique label	
	% For LaTeX tables use	
	\resizebox{\textwidth}{!}{
	\begin{tabular}{llll}		
		\hline\noalign{\smallskip}		
		\textbf{HPO method} & \textbf{Advantage} & \textbf{Disadvantage} & \textbf{Application} \\		
		\noalign{\smallskip}\hline\noalign{\smallskip}		
		Grid Search & \tabincell{l}{- simple \\ - enable parallelization \\ - Reliability in \\low-dimensional space} & \tabincell{l}{- time consuming \\ - Poor high-dimensional \\space efficiency} &  \\	
		\noalign{\smallskip}\hline
		Random Search & \tabincell{l}{- enable parallelization \\ - More efficient than\\ grid search} & \tabincell{l}{- No guarantee of \\optimal results \\ - Don't use the previous \\sampling configuration} &  \\
		\noalign{\smallskip}\hline
		BO-GP & \tabincell{l}{- Support continuous and \\discrete hyperparameters  \\ - Reliable and promising}& \tabincell{l}{- Not efficient with \\conditional HPs. \\ - Difficult for parallelism \\ - Poor scalability of\\ high-dimensional space } & \\
		\noalign{\smallskip}\hline
		SMAC & \tabincell{l}{- Applicable to almost \\ all hyperparameters} & \tabincell{l}{- Difficult for parallelism \\- Poor scalability of\\ high-dimensional space} & \\
		\noalign{\smallskip}\hline
		TPE & \tabincell{l}{- Applicable to almost \\ all hyperparameters} & \tabincell{l}{- Difficult for parallelism \\- Poor scalability of\\ high-dimensional space} & \\
		\noalign{\smallskip}\hline
		GA & \tabincell{l}{- Applicable to almost \\ all hyperparameters \\ - Combine with other\\ algorithms} & \tabincell{l}{- premature convergence\\- New hyperparameters } & \\
		\noalign{\smallskip}\hline
		CMA-ES &\tabincell{l}{- enable parallelization} & applicability limited  & \\
		\noalign{\smallskip}\hline
		PSO & \tabincell{l}{- enable parallelization \\Applicable to almost \\ all hyperparameters } & \tabincell{l}{- Sensitive to \\initialization } & \\
		\noalign{\smallskip}\hline
		ACO & \tabincell{l}{- enable parallelization \\ - Combine with other\\ algorithms }& \tabincell{l}{- slow initial \\convergence rate \\ Local optima } & \tabincell{l}{- Less used for \\hyperparameter optimization} \\
		\noalign{\smallskip}\hline
		PBT & \tabincell{l}{- Combination of parallel search\\ and sequential optimization\\- enable parallelization \\ } & \tabincell{l}{- glass box \\implementation} & \tabincell{l}{Suitable for GAN \\network and computationally\\ expensive networks} \\
		\noalign{\smallskip}\hline
		SHA & \tabincell{l}{- robustness and \\versatility} & \tabincell{l}{- $n\quad vs.\quad B/n$ \\problem} & \\
		\noalign{\smallskip}\hline
		HB & \tabincell{l}{- Flexibility and \\scalability of\\ high-dimensional space \\- enable parallelization} & \tabincell{l}{- Don't use the previous \\sampling configuration} & \\
		\noalign{\smallskip}\hline
		BOHB & \tabincell{l}{- enable parallelization\\- robustness and \\versatility} & \tabincell{l}{- require meaningful \\small budget} & \\
		\noalign{\smallskip}\hline
	\end{tabular}}
\end{table}


\section{Hyper-parameter optimization Application}
\label{sec:Application}

\subsection{Conventional machine learning models}

To boost machine learning models by HPO, firstly, we need to find out what the key hyper-parameters are that people need to tune to fit the machine learning models into specific problems or datasets.

In general, most of machine learning models are defined as supervised learning algorithms.
Supervised learning algorithms are a set of machine learning algorithms that map input features to a target by training on labeled data, and mainly include linear models, 
k-nearest neighbors (KNN), 
support vector machines (SVM), 
naíve Bayes (NB), 
and decision-tree-based models.\citep{10.1145/1143844.1143865}

\paragraph{Linear Models}

In general, supervised learning models can be classified as regression and classification techniques when used to predict continuous or discrete target variables, respectively. 
Linear regression \citep{bishop2006pattern} is a typical regression model that predicts a target y by the following equation:
\begin{equation}
	\begin{aligned}
		\hat{y}(\mathbf{w}, \mathbf{x})=w_{0}+w_{1} x_{1}+\ldots+w_{p} x_{p}
	\end{aligned}	
\end{equation}

Usually, no hyper-parameter needs to be tuned in linear regression. 
A linear model’s performance mainly depends on how well the problem or data follows a linear distribution.

To improve the original linear regression models, ridge regression was proposed in \citep{hoerl1970ridge}. 
Ridge regression imposes a penalty on the coefficients, and aims to minimize the objective function \citep{melkumova2017comparing}:
\begin{equation}
	\begin{aligned}
		\alpha\|w\|_{2}^{2}+\sum_{i=1}^{p}\left(y_{i}-w_{i} \cdot x_{i}\right)^{2}
	\end{aligned}	
\end{equation}

Lasso regression \citep{tibshirani1996regression} is another linear model used to estimatesparse coefficients, consisting of a linear model with an $L_{1}$ priori added regularization term. It aims to minimize the objective function:
\begin{equation}
	\begin{aligned}
		\alpha\|w\|_{1}+\sum_{i=1}^{p}\left(y_{i}-w_{i} \cdot x_{i}\right)^{2}
	\end{aligned}	
\end{equation}

where $\alpha$ is the regularization strength and $\|w\|_{1}$ is the $L_{1}$-norm of the coefficient vector. 
Therefore, the regularization strength $\alpha$ is an crucial hyper-parameter of both ridge and lasso regression models.
Typically, the $\alpha$ value is set manually, or a simple grid search or random search is \citep{abreu2019automated}.

Logistic regression (LR) \citep{hosmer2013applied} is a linear model used for classification problems. 
In LR, its cost function may be different, depending on the regularization method chosen for the penalization. 
There are three main types of regularization methods in LR: $L_{1}$-norm, $L_{12}$-norm, and elastic-net regularization \citep{ogutu2012genomic}.

\paragraph{K-nearest neighbor (KNN)}
 is a simple machine learning algorithm that is used to classify data points by calculating the distances between different data points.
In KNN, the predicted class of each test sample is set to the class to which most of its k-nearest neighbors in the training set belong,  its class y can be denoted by \citep{keller1985fuzzy}:
 
\begin{equation}
	\begin{aligned}
		 y=\arg \max _{c_{j}} \sum_{x_{i} \in N_{k}(x)} I\left(y_{i}=c_{j}\right), \quad i=1,2, \ldots, n ; j=1,2, \ldots, m,
	\end{aligned}	
\end{equation}

In KNN, the number of considered nearest neighbors, k, is the most crucial hyper-parameter \citep{drucker1996support}. 
If k is too small, the model will be under-fitting; 
if k is too large, the model will be over-fitting and require high computational time. 
In addition, the weighted function used in the prediction can also be chosen from 'uniform'(points are weighted equally) or‘distance’ (points are weighted by the inverse of their distance), depending on specific problems. 
The distance metric and the power parameter of the Minkowski metric can also be tuned as it can result in minor improvement. 
Lastly, the 'algorithm' used to compute the nearest neighbors can also be chosen from a ball tree, a k-dimensional (KD) tree, or a brute force search. 
Typically, the model can determine the most appropriate algorithm itself by setting the 'algorithm' to 'auto' in sklearn.

\paragraph{Support Vector Machines (SVM)} is a supervised learning algorithm that can be used for both classification and regression problems \citep{drucker1996support}. 
SVM algorithms are based on the concept of mapping data points from low-dimensional into high-dimensional space to make them linearly separable; 
a hyperplane is then generated as the classification boundary to partition data points. 
Assuming there are n data points, the objective function of SVM is:
\begin{equation}
	\begin{aligned}
		\arg \min _{\mathbf{w}}\left\{\frac{1}{n} \sum_{i=1}^{n} \max \left\{0,1-y_{i} f\left(x_{i}\right)\right\}+C \mathbf{w}^{T} \mathbf{w}\right\},	
	\end{aligned}	
\end{equation}
where $\mathbf{w}$ is a normalization vector; 
C is the penalty parameter of the error term, which is an important hyper-parameter of all SVM models.
The kernel function f(x), which is used to measure the similarity between two data points $x_{i}$ and $x_{j}$, can be chosen from multiple types of kernels in SVM models. 
Therefore, the kernel type would be a vital hyper-parameter to be tuned. 
Common kernel types in SVM include linear kernels, radial basis function (RBF), polynomial kernels, and sigmoid kernels\citep{soliman2012classification}.

Moreover, the polynomial kernel has an additional conditional hyper-parameter d representing the 'degree' of the polynomial kernel function. 
In support vector regression (SVR) models, there is another hyper-parameter, 'epsilon', indicating the distance error to of its loss function.

\paragraph{Naíve Bayes (NB)} are supervised learning algorithms based on Bayes theorem. Assuming there are n dependent features $x_{1}$ . . . $x_{n}$ and a target variable y, the objective function of naíve Bayes can be denoted \citep{rish2001empirical}:
 
\begin{equation}
	\begin{aligned}
		\hat{y}=\arg \max _{y} P(y) \prod_{i=1}^{n} P\left(x_{i} \mid y\right)
	\end{aligned}	
\end{equation}
where P(y) is the probability of a value y, and P($x_{i}$|y) is the posterior probabilities of $x_{i}$ given the values of y. 
Regarding the different assumptions of the distribution of P($x_{i}$|y), there are different types of naíve Bayes classifiers. 
The four main types of NB models are: Bernoulli NB, Gaussian NB, multinomial NB, and complement NB\citep{sulzmann2007pairwise}. 
They both have the additive (Laplace/Lidstone) smoothing parameter $\alpha$, as the main hyper-parameter that needs tuning. 
To conclude, for naíve Bayes algorithms, developers often do not need to tune hyper-parameters or only need to tune the smoothing parameter $\alpha$, which is a continuous hyper-parameter.

\paragraph{Tree-based models}
Decision tree (DT)\citep{safavian1991survey} is a common classification method that uses a tree-structure to model decisions and possible consequences by summarizing a set of classification rules from the data. 
A DT has three main components: a root node representing the entire data; 
multiple decision nodes indicating decision tests and sub-node splits over each feature; 
and several leaf nodes representing the result classes\citep{manias2019machine}. 
DT algorithms recursively split the training set with better feature values to achieve good decisions on each subset. 
Pruning, which means removing some of the sub-nodes of decision nodes, is used in DT to avoid over-fitting. 
Since a deeper tree has more sub-trees to make more accurate decisions, the maximum tree depth, is an essential hyper-parameter that controls the complexity of DT algorithms\citep{yang2019tree}.

\hspace*{\fill} \ 

In addition, traditional machine learning also has some unsupervised learning models, include K-Means Clustering, Principal Component Analysis(PCA)etc.
Unliking supervised learning above there is no correct answers and there is no teacher. 
Algorithms are left to their own devises to discover and present the interesting structure in the data. 
The unsupervised learning algorithms learn few features from the data. 
When new data is introduced, it uses the previously learned features to recognize the class of the data. 
Therefore, it is mainly used for clustering and feature reduction.

\paragraph{K-Means Clustering}
The K-means algorithm is a popular data-clustering algorithm.\citep{hartigan1979algorithm}
The procedure follows a simple and easy way to classify a given data set through a certain number of clusters. 
The main idea is to define k centers, one for each cluster. 
These centers should be placed in a cunning way because of different location causes different result. 
So, the better choice is to place them is much as possible far away from each other.

Commonly used initialization methods are Forgy and Random Partition.\citep{hamerly2002alternatives}
The Forgy method randomly chooses k observations from the dataset and uses these as the initial means. 
The Random Partition method first randomly assigns a cluster to each observation and then proceeds to the update step, thus computing the initial mean to be the centroid of the cluster's randomly assigned points. 
The Forgy method tends to spread the initial means out, while Random Partition places all of them close to the center of the data set.


The next step is to calculate local minima of the minimum-sum-of-squares clustering: 
\begin{equation}
	\begin{aligned}
		\underset{\mathbf{S}}{\arg \min } \sum_{i=1}^{k} \sum_{\mathbf{x} \in  S_{i}}\left\|\mathbf{x}-\boldsymbol{\mu}_{i}\right\|^{2}=\underset{\mathbf{s}}{\arg \min } \sum_{i=1}^{k}\left|S_{i}\right| \operatorname{Var} S_{i}
	\end{aligned}	
\end{equation}
Given a set of observations ($x_1, x_2,$ ..., $x_n$), where each observation is a d-dimensional real vector, k-means clustering aims to partition the n observations into k ($\le$ n) sets S = {$S_1, S_2,$ ..., $S_k$} so as to minimize the within-cluster sum of squares (WCSS).\citep{kriegel2017black}

\paragraph{Principal component analysis (PCA)}
is probably the most popular multivariate statistical technique and it is used by almost all scientific disciplines. 
It is also likely to be the oldest multivariate technique.
It is often used as a dimensionality-reduction technique.\citep{yang2020hyperparameter}

One significant challenge in using PCA is the choice of the number of components.
The number of components (e.g. a hypothesis saying the number of dimensions must be around K or so) tuning it is proved to be perfectly valid.\citep{choi2017selecting}


\subsection{Deep learning models}

Deep learning is a new research direction in the field of machine learning. It learns the inherent laws and representation levels of sample data. The information obtained during these learning processes is of great help to the interpretation of data such as text, images and sounds. Its ultimate goal is to enable machines to have the ability to analyze and learn like humans, and to recognize data such as words, images and sounds. Nowadays, deep learning has been widely used in various fields such as computer vision, natural language processing, and recommender systems . Common types of DL models include deep belief networks(DBNs), deep neural networks(DNNs), convolutional neural networks(CNNs), recurrent neural networks(RNNs), and more \citep{dargan2020survey}.Almost all DL models have similar hyperparameters because they have similar underlying neural network structures. A deep-learning architecture is a multilayer stack of simple modules, containing three types of hierarchies: input layer, hidden layer, and output layer, where most of which are subject to learning, many of which compute non-linear input–output mappings \citep{lecun2015deep}. Compared with conventional machine learning, there are more hyperparameters in deep learning models that need to be adjusted, which means it is more suitable for hyperparameter optimization algorithms. 

\paragraph{Convolutional Neural Network}

Convolutional Neural Networks (CNN) is a type of feedforward neural network that includes convolutional computation and has a deep structure. It is one of the representative algorithms of deep learning (if you want to learn more about convolutional neural network, please refer to \citep{albawi2017understanding}). In machine learning hyperparameter optimization, deep learning algorithms such as convolutional neural networks usually have many hyper-parameters with various, different types, and the search space is generally very high-dimensional. Thus, the
evaluation of a given hyperparameter setting can take a considerable amount of time and storage \citep{hinz2018speeding}.

Random search can be used to quickly identify promising regions in the hyperparameter space. Although there is a possibility of getting stuck in local optima, this has important implications for us to narrow the hyperparameter space. Random Search, Bayesian Optimization approaches and population-based optimization algorithms such as Genetic Algorithms , Evolutionary Algorithms and Particle Swarm Optimization have been
successfully used to select the best CNN hyper-parameters. \citep{hinz2018speeding,gulcu2021multi,lorenzo2017particle}. Especially, EAs are among the most widely used techniques for HPO. In \citep{real2017large,suganuma2017genetic,elsken2018efficient} EAs have been used for optimizing the network architecture and gradient-based methods have been used for optimizing the network weights. 

However, particle swarm optimization is catching up with evolutionary algorithms due to issues with parallelization and premature convergence. Although evolutionary algorithms also have variants that can be parallelized, particle swarm optimization is still more efficient than evolutionary algorithms \citep{serizawa2020optimization,yamasaki2017efficient}. 

\paragraph{Long Short Term Memory}

Long short term memory(LSTM) \citep{hochreiter1997long} is a special recurrent neural network(RNN), which is used to solve the "gradient vanishing" problem in the RNN structure of the recurrent neural network. It is suitable for processing and predicting important events with relatively long intervals and delays in time series. Similar to convolutional neural networks, random search, Bayesian optimization and other methods are also applicable to long short term memory. Among them, evolutionary algorithms are widely used due to their diversity of solutions and fast convergence characteristics \citep{angeline1994evolutionary,nakisa2018long}. In some aspects, the performance is even better than particle swarm optimization. 

\paragraph{Generative Adversarial Networks}

Generative Adversarial Networks (GANs) provide a way to learn deep representations without requiring large amounts of labeled training data. Due to the huge amount of data in GANs, there will be expensive costs in the process of hyperparameter optimization.The previous hyperparameter optimization algorithms selected the optimal model after each iteration convergence, while the PBT algorithm can adjust the hyperparameters during the iteration process, so it is very suitable for GANs.(If you want to get more information on GANs, please see the paper \citep{creswell2018generative})