\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\thispagestyle {empty}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Preliminaries}{7}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Learning Rate}{8}{paragraph*.3}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Batch Size}{8}{paragraph*.4}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Epoch}{10}{paragraph*.5}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Optimizer}{10}{paragraph*.7}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Batch gradient descent}{11}{paragraph*.8}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Stochastic Gradient Descent}{11}{paragraph*.9}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Mini-batch gradient descent}{12}{paragraph*.11}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Momentum}{13}{paragraph*.12}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Nesterov accelerated gradient}{14}{paragraph*.15}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Adagrad}{15}{paragraph*.17}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Adadelta}{15}{paragraph*.18}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{RMSprop}{16}{paragraph*.19}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Adam}{16}{paragraph*.20}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Paper collection}{19}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Survey Scope}{19}{section.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Paper Collection Methodology}{20}{section.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Collection Result}{21}{section.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Paper Organisation}{21}{section.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{1) Literature Review.}{22}{paragraph*.28}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{2) Statistical Analysis and Summary.}{23}{paragraph*.29}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Summary of parameter and Hyperparameter adjustment methods}{24}{chapter.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Hyperparameter Initialization Method}{24}{section.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Activation Function}{25}{subsection.4.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Sigmoid/Logistic Sigmoid Function}{25}{paragraph*.31}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{ReLU Function}{25}{paragraph*.32}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Parametric ReLU Function}{26}{paragraph*.33}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Weight Initialization}{27}{subsection.4.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Xavier Initialization}{27}{paragraph*.34}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Kaming He Initialization}{28}{paragraph*.35}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}Conclusion}{28}{subsection.4.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Hyperparameter Optimization Method}{28}{section.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Black box optimization}{29}{subsection.4.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Grid Search}{30}{paragraph*.37}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Random Search}{31}{paragraph*.39}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Bayesian Optimization}{33}{paragraph*.40}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Population-based Algorithm}{38}{paragraph*.41}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Multi-fidelity Model}{42}{subsection.4.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Learning Curve}{42}{paragraph*.42}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Bandit Algorithm}{44}{paragraph*.43}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Other methods}{46}{subsection.4.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Gradient-based optimization}{46}{paragraph*.44}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Hybrid Methodology}{47}{paragraph*.46}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Application of hyperparameter optimization method}{49}{chapter.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Comparison of optimization methods}{49}{section.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Hyper-parameter optimization Application}{54}{section.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Conventional machine learning models}{54}{subsection.5.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Linear Models}{54}{paragraph*.48}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{K-nearest neighbor (KNN)}{55}{paragraph*.49}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Support Vector Machines (SVM)}{55}{paragraph*.50}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Naíve Bayes (NB)}{56}{paragraph*.51}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Tree-based models}{57}{paragraph*.52}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{K-Means Clustering}{57}{paragraph*.53}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Principal component analysis (PCA)}{58}{paragraph*.54}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}Deep learning models}{58}{subsection.5.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Convolutional Neural Network}{59}{paragraph*.55}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Long Short Term Memory}{60}{paragraph*.56}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Generative Adversarial Networks}{60}{paragraph*.57}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Hyperparameter Optimization Framework}{61}{chapter.6}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Scikit-learn}{61}{section.6.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}HyperOpt}{62}{section.6.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}BayesOpt}{62}{section.6.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.4}Google Vizier}{62}{section.6.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.5}Ray Tune}{62}{section.6.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.6}GPflowOpt}{63}{section.6.6}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.7}Skopt}{63}{section.6.7}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.8}Autotune}{63}{section.6.8}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.9}Optuna}{63}{section.6.9}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Current Issues, Challenges, and Future Research Directions }{65}{chapter.7}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}Benchmarks and Comparability}{65}{section.7.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2}Overfitting and Generalization}{66}{section.7.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.3}Scalability}{66}{section.7.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.4}Continuous updating capability}{67}{section.7.4}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Acknowledgements}{68}{chapter*.60}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{References}{69}{chapter*.61}%
